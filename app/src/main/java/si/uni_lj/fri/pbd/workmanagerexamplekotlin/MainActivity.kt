package si.uni_lj.fri.pbd.workmanagerexamplekotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.work.*
import si.uni_lj.fri.pbd.workmanagerexamplekotlin.NotificationWorker.Companion.ARG1
import si.uni_lj.fri.pbd.workmanagerexamplekotlin.NotificationWorker.Companion.ARG2
import si.uni_lj.fri.pbd.workmanagerexamplekotlin.NotificationWorker.Companion.ARG3
import si.uni_lj.fri.pbd.workmanagerexamplekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        val workManager = WorkManager.getInstance(this)

        // TODO: Set constraints
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .build()


        val calcData: Data = workDataOf(ARG1 to 32,
                                            ARG2 to 3,
                                            ARG3 to 7)

        // TODO: Make request
        val request = OneTimeWorkRequest.Builder(NotificationWorker::class.java)
            .setConstraints(constraints)
            .setInputData(calcData)
            .build()

        binding.button.setOnClickListener {
            // TODO: Enqueue request
            workManager.enqueue(request)
        }

        // TODO: Keep track of the result
        workManager.getWorkInfoByIdLiveData(request.id)
            .observe(this, object: Observer<WorkInfo>{
                override fun onChanged(workInfo: WorkInfo?) {
                    val state = workInfo?.state
                    binding.textView.append(state.toString() + "\n")
                    if (state?.isFinished == true) {
                        binding.resultView.text = workInfo.outputData.getDouble(NotificationWorker.CALC_RESULT,
                            0.0
                        ).toString()
                    }
                }
            })

    }
}