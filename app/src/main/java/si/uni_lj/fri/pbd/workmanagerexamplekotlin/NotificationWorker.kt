package si.uni_lj.fri.pbd.workmanagerexamplekotlin

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import kotlin.math.pow

class NotificationWorker(appContext: Context, workerParams: WorkerParameters):
    Worker(appContext, workerParams) {

    companion object{
        const val ARG1 = "X"
        const val ARG2 = "Y"
        const val ARG3 = "Z"
        const val CALC_RESULT = "result"

    }

    override fun doWork(): Result {

        val x = inputData.getInt(ARG1, 0)
        val y = inputData.getInt(ARG2, 0)
        val z = inputData.getInt(ARG3, 0)

        val result = x.toDouble().pow(y.toDouble()) /z.toDouble()

        showNotification("WorkManager",result.toString())

        val output = workDataOf(CALC_RESULT to result)

        return Result.success(output)
    }

    private fun showNotification(taskName: String, result: String) {
        val manager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "task_channel"
        val channelName = "task_name"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }
        val builder =
            NotificationCompat.Builder(applicationContext, channelId)
                .setContentTitle(taskName)
                .setContentText(result)
                .setSmallIcon(R.mipmap.ic_launcher)
        manager.notify(1, builder.build())
    }

}